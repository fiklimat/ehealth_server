[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Server.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Server.App_Start.NinjectWebCommon), "Stop")]

namespace Server.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using Ninject.Web.WebApi;
    using Server.Core;
    using Server.Services;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application.
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        
        /// propojení interface a service
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICommonService>().To<CommonService>();
            kernel.Bind<CommonService>().To<CommonService>().InSingletonScope();
            kernel.Bind<IMessageService>().To<MessageService>();
            kernel.Bind<MessageService>().To<MessageService>().InSingletonScope();
            kernel.Bind<IMSAService>().To<MSAService>();
            kernel.Bind<MSAService>().To<MSAService>().InSingletonScope();
            kernel.Bind<IMSHService>().To<MSHService>();
            kernel.Bind<MSHService>().To<MSHService>().InSingletonScope();
            kernel.Bind<IOBRService>().To<OBRService>();
            kernel.Bind<OBRService>().To<OBRService>().InSingletonScope();
            kernel.Bind<IOBXService>().To<OBXService>();
            kernel.Bind<OBXService>().To<OBXService>().InSingletonScope();
            kernel.Bind<IORCService>().To<ORCService>();
            kernel.Bind<ORCService>().To<ORCService>().InSingletonScope();
            kernel.Bind<IPIDService>().To<PIDService>();
            kernel.Bind<PIDService>().To<PIDService>().InSingletonScope();
            kernel.Bind<IPV1Service>().To<PV1Service>();
            kernel.Bind<PV1Service>().To<PV1Service>().InSingletonScope();
            kernel.Bind<ITermsService>().To<TermsService>();
            kernel.Bind<TermsService>().To<TermsService>().InSingletonScope();
        }
    }
}