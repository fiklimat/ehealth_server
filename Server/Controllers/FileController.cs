﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class FileController : BaseController
    {
        /// <summary>
        /// Kontroler pro nahrání dat do databáze
        /// </summary>
        private IMessageService messageService;
        private IMSAService msaService;
        private IMSHService mshService;
        private IOBRService obrService;
        private IOBXService obxService;
        private IORCService orcService;
        private IPIDService pidService;
        private IPV1Service pv1Service;
        private ITermsService termsService;

        public FileController(ICommonService commonService, IMessageService messageService, IMSAService msaService, IMSHService mshService, IOBRService obrService, IOBXService obxService, IORCService orcService, IPIDService pidService, IPV1Service pv1Service, ITermsService termsService) : base(commonService)
        {
            // inicializace kotnroleru
            this.messageService = messageService;
            this.msaService = msaService;
            this.mshService = mshService;
            this.obrService = obrService;
            this.obxService = obxService;
            this.orcService = orcService;
            this.pidService = pidService;
            this.pv1Service = pv1Service;
            this.termsService = termsService;
        }

        // endpoint pro nahrání originálních zpráv do databáze
        // struktura databáze odpovídá struktuře HL7 zprávy (obsahuje tabulky pro všechny MSH, OBR, OBX atd.) a navíc obsahuje tabulku message, která udržuje id záznamů patřících k jedné zprávě
        // prochází soubory po řádkách a postupně vytváří záznamy v databázi pomocí services
        [HttpGet]
        [ActionName("loadFiles")]
        public HttpResponseMessage LoadFiles()
        {
            foreach (string file in Directory.EnumerateFiles(@"D:\škola\7 semestr\ehealth\ukol\by_id", "*.txt")) //cesta k souborům se zprávami
            {
                string[] lines = File.ReadAllText(file).Split(new string[] { "\n", "\r", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                bool first_MSH = true;
                int msh1_id = default;
                int msh2_id = default;
                int pid_id = default;
                int pv1_id = default;
                int orc_id = default;
                int obr_id = default;
                int msa_id = default;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].Contains("MSH") && first_MSH)
                    {
                        first_MSH = false;
                        msh msh = mshService.Create(lines[i]);
                        msh1_id = msh.id;
                    }
                    else if (lines[i].Contains("MSH") && !first_MSH)
                    {
                        first_MSH = true;
                        msh msh = mshService.Create(lines[i]);
                        msh2_id = msh.id;
                    }
                    if (lines[i].Contains("PID"))
                    {
                        pid_id = pidService.Create(lines[i]).id;
                    }
                    if (lines[i].Contains("PV1"))
                    {
                        pv1_id = pv1Service.Create(lines[i]).id;
                    }
                    if (lines[i].Contains("ORC"))
                    {
                        orc_id = orcService.Create(lines[i]).id;
                    }
                    if (lines[i].Contains("OBR"))
                    {
                        obr_id = obrService.Create(lines[i]).id;
                    }
                    if (lines[i].Contains("OBX"))
                    {
                        obxService.Create(lines[i], msh1_id);
                    }
                    if (lines[i].Contains("MSA"))
                    {
                        msa_id = msaService.Create(lines[i]).id;
                        message message_new = new message();
                        message_new.msh1_id = msh1_id;
                        message_new.msh2_id = msh2_id;
                        message_new.pid_id = pid_id;
                        message_new.pv1_id = pv1_id;
                        message_new.orc_id = orc_id;
                        message_new.obr_id = obr_id;
                        message_new.msa_id = msa_id;
                        messageService.Create(message_new);
                    }
                }
            }
            return commonService.GetResponse(200);
        }

        // endpoint pro nahrání terminologie ze souboru do databáze
        [HttpGet]
        [ActionName("loadTerminology")]
        public HttpResponseMessage LoadTerminology()
        {
            string[] lines = File.ReadAllText(@"D:\škola\7 semestr\ehealth\ukol\Terminology.txt").Split(new string[] { "\n", "\r", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            termsService.UpdateTerminology(lines);
            return commonService.GetResponse(200);
        }
    }
}