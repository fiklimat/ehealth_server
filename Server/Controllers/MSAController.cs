﻿using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class MSAController : BaseController
    {
        /// <summary>
        /// Kontroler pro přijetí MSA zprávy
        /// </summary>
        public MSAController(ICommonService commonService) : base(commonService)
        {
            // inicializace kotnroleru
        }

        //endpoint přijímající a logující MSA zprávu
        [HttpPost]
        [ActionName("sendMSA")]
        public HttpResponseMessage sendMSA(HttpRequestMessage msa_message)
        {
            string message = msa_message.Content.ReadAsStringAsync().Result;
            message.logMessageHL7();
            return commonService.GetResponse(200);
        }
    }
}