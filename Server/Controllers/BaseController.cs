﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Server.Controllers
{
    /// <summary>
    /// Base kontroler, ze kterého vychází všechny ostatní kontrolery
    /// </summary>
    public class BaseController : ApiController
    {
        public ICommonService commonService;
        public BaseController(ICommonService commonService)
        {
            this.commonService = commonService;
        }
    }
}