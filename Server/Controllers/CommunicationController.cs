﻿using Newtonsoft.Json.Linq;
using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace Server.Controllers
{
    public class DiagnosticReport 
    { 
        /// <summary>
        /// objekt s formátem Diagnostic Report zprávy
        /// </summary>
        public string resourceType { get; set; }
        public Identifier identifier { get; set; }
        public string status { get; set; }
        public Code code { get; set; }
        public Subject subject { get; set; }
        public EffectivePeriod effectivePeriod { get; set; }
    }
    public class Observation
    {
        /// <summary>
        /// objekt s formátem Observation zprávy
        /// </summary>
        public string resourceType { get; set; }
        public Identifier identifier { get; set; }
        public string status { get; set; }
        public Code code { get; set; }
        public Subject subject { get; set; }
        public string effectiveDateTime { get; set; }
        public ValueQuantity valueQuantity { get; set; }

    }
    public class Identifier
    { 
        public string value { get; set; }
    }
    public class Code
    {
        public Coding[] coding { get; set; }
    }
    public class Coding
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }
    public class Subject
    {
        public string resourceType { get; set; }
        public Identifier[] identifier { get; set; }
    }
    public class EffectivePeriod
    {
        public string start { get; set; }
        public string end { get; set; }
    }
    public class ValueQuantity
    {
        public string value { get; set; }
        public string unit { get; set; }
    }

    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class CommunicationController : BaseController
    {
        /// <summary>
        /// Kontroler pro komunikaci pomocí HL7 a FHIR
        /// </summary>
        private IMessageService messageService;
        private IMSHService mshService;
        private IOBRService obrService;
        private IOBXService obxService;
        private IORCService orcService;
        private IPIDService pidService;
        private IPV1Service pv1Service;

        public CommunicationController(ICommonService commonService, IMessageService messageService, IMSHService mshService, IOBRService obrService, IOBXService obxService, IORCService orcService, IPIDService pidService, IPV1Service pv1Service) : base(commonService)
        {
            // inicializace kotnroleru
            this.messageService = messageService;
            this.mshService = mshService;
            this.obrService = obrService;
            this.obxService = obxService;
            this.orcService = orcService;
            this.pidService = pidService;
            this.pv1Service = pv1Service;
        }

        //endpoint pro přijetí ORM zprávy a odeslání HL7 zprávy
        [HttpPost]
        [ActionName("getDataHL7")]
        public HttpResponseMessage GetDataHL7(HttpRequestMessage orm_message)
        {
            // převedení ORM na string a rozdělení na řádky
            string message = orm_message.Content.ReadAsStringAsync().Result;
            message.logMessageHL7(); // zalogování ORM do souboru
            string[] lines = message.Split(new string[] { "\n", "\r", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            string patient_identifier_list = null;
            string[] terms = null;
            string start_time = null;
            string end_time = null;

            // získání id pacienta, počáteční a konečný čas a požadované parametry
            foreach (string line in lines)
            {
                if (line.Contains("PID"))
                {
                    string[] arr = line.Split('|');
                    patient_identifier_list = arr[3];
                }
                if (line.Contains("OBR"))
                {
                    string[] arr = line.Split('|');
                    terms = arr[4].Split('~');
                    start_time = arr[7];
                    end_time = arr[8];
                }
            }
            // získání HL7 zprávy z messageService
            string result = messageService.GetHL7Message(patient_identifier_list, terms, start_time, end_time);
            result.logMessageHL7(); // zalogování zprávy do souboru
            return commonService.GetResponse(result);
        }

        //endpoint pro přijetí Diagnostic Report zprávy a odeslání HL7 FHIR zprávy
        [HttpPost]
        [ActionName("getDataHL7FHIR")]
        public HttpResponseMessage GetDataHL7FHIR(HttpRequestMessage diagnosticReport)
        {
            // převedení zprávy na objekt Diagnostic Report
            string message = diagnosticReport.Content.ReadAsStringAsync().Result;
            message.logMessageHL7FHIR(); // zalogování zprávy do souboru
            DiagnosticReport report = new JavaScriptSerializer().Deserialize<DiagnosticReport>(message);

            // získání Observation zpráv z messageService
            List<Observation> observations = messageService.GetHL7FHIRMessage(report);

            // převedení na JSON a následně na string
            JArray array = observations.observationsToJson();
            string response = array.ToString();
            response.logMessageHL7FHIR(); // zalogování zprávy do souboru
            return commonService.GetResponse(response);
        }
    }
}