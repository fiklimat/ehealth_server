﻿using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class PatientController : BaseController
    {
        /// <summary>
        /// Kontroler pro posílání dat o pacientech
        /// </summary>
        private IPIDService pidService;

        public PatientController(ICommonService commonService, IPIDService pidService) : base(commonService)
        {
            // inicializace kotnroleru
            this.pidService = pidService;
        }

        // endpoint pro odeslání všech pacientských id
        [HttpGet]
        [ActionName("allIds")]
        public HttpResponseMessage loadPatientIds()
        {
            List<string> ids = pidService.GetIds();
            return commonService.GetResponse(ids.listToString());
        }
    }
}