﻿using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class TimesController : BaseController
    {
        /// <summary>
        /// Kontroler pro posílání časů jednotlivých měření
        /// </summary>
        private IOBRService obrService;

        public TimesController(ICommonService commonService, IOBRService obrService) : base(commonService)
        {
            // inicializace kontroleru
            this.obrService = obrService;
        }

        // endpoint pro posílání všech časů měření konkrétního pacienta
        [HttpGet]
        public HttpResponseMessage LoadTimesForPatient(string id)
        {

            List<string> times = obrService.GetTimesForPatient(id);
            return commonService.GetResponse(times.listToString());
        }
    }
}