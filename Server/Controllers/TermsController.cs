﻿using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class TermsController : BaseController
    {
        /// <summary>
        /// Kontroler pro posílání terminologických termínů
        /// </summary>
        private ITermsService termsService;

        public TermsController(ICommonService commonService, ITermsService termsService) : base(commonService)
        {
            // inicializace kontroleru
            this.termsService = termsService;
        }

        // endpoint pro odeslání všech termínů
        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetAll()
        {
            List<terms> terms = termsService.GetAll();
            return commonService.GetResponse(terms.termsToString());
        }
    }
}