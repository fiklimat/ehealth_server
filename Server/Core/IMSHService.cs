﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IMSHService
    {
        /// <summary>
        /// Interface pro MSHService
        /// </summary>
        msh Create(string line);
    }
}