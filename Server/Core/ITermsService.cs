﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface ITermsService
    {
        /// <summary>
        /// Interface pro TermsService
        /// </summary>
        List<terms> GetAll();
        bool UpdateTerminology(string[] lines);
    }
}