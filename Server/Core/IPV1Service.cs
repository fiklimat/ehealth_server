﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IPV1Service
    {
        /// <summary>
        /// Interface pro PV1Service
        /// </summary>
        pv1 Create(string line);
    }
}