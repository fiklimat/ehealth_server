﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IOBXService
    {
        /// <summary>
        /// Interface pro OBXService
        /// </summary>
        obx Create(string line, int msh_id);
    }
}