﻿using Server.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IMessageService
    {
        /// <summary>
        /// Interface pro MessageService
        /// </summary>
        message Create(message message_new);
        string GetHL7Message(string patient_identifier_list, string[] terms, string start_time, string end_time);
        List<Observation> GetHL7FHIRMessage(DiagnosticReport diagnosticReport);
    }
}