﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IORCService
    {
        /// <summary>
        /// Interface pro ORCService
        /// </summary>
        orc Create(string line);
    }
}