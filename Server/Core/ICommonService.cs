﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Server.Core
{
    public interface ICommonService
    {
        /// <summary>
        /// Interface pro CommonService
        /// </summary>
        HttpResponseMessage GetResponse(int statusCode);
        HttpResponseMessage GetResponse(string content);
        HttpResponseMessage GetResponse(string content, int statusCode);
    }
}