﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IOBRService
    {
        /// <summary>
        /// Interface pro OBRService
        /// </summary>
        obr Create(string line);
        List<string> GetTimesForPatient(string id);
    }
}