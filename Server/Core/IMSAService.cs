﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IMSAService
    {
        /// <summary>
        /// Interface pro MSAService
        /// </summary>
        msa Create(string line);
    }
}