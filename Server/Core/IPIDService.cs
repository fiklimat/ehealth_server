﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Core
{
    public interface IPIDService
    {
        /// <summary>
        /// Interface pro PIDService
        /// </summary>
        pid Create(string line);
        List<string> GetIds();
    }
}