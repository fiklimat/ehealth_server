namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("message")]
    public partial class message
    {
        /// <summary>
        /// model tabulky message
        /// </summary>
        public int id { get; set; }

        public int? msh1_id { get; set; }

        public int? msh2_id { get; set; }

        public int? pid_id { get; set; }

        public int? pv1_id { get; set; }

        public int? orc_id { get; set; }

        public int? obr_id { get; set; }

        public int? msa_id { get; set; }

        public virtual msh msh { get; set; }

        public virtual msh msh1 { get; set; }

        public virtual pid pid { get; set; }

        public virtual pv1 pv1 { get; set; }

        public virtual orc orc { get; set; }

        public virtual obr obr { get; set; }

        public virtual msa msa { get; set; }
    }
}
