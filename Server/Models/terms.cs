namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("terms")]
    public partial class terms
    {
        /// <summary>
        /// model tabulky terms
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public terms()
        {
            obx = new HashSet<obx>();
        }

        [Key]
        [StringLength(255)]
        public string original { get; set; }

        [StringLength(255)]
        public string snomed { get; set; }

        [StringLength(255)]
        public string loinc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<obx> obx { get; set; }
    }
}
