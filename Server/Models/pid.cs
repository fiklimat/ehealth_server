namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pid")]
    public partial class pid
    {
        /// <summary>
        /// model tabulky PID
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pid()
        {
            message = new HashSet<message>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string set_id_pid { get; set; }

        [StringLength(255)]
        public string patient_id { get; set; }

        [Required]
        [StringLength(255)]
        public string patient_identifier_list { get; set; }

        [StringLength(255)]
        public string alternate_patient_id_pid { get; set; }

        [Required]
        [StringLength(255)]
        public string patient_name { get; set; }

        [StringLength(255)]
        public string mothers_maiden_name { get; set; }

        [StringLength(255)]
        public string date_time_birth { get; set; }

        [StringLength(255)]
        public string administrative_sex { get; set; }

        [StringLength(255)]
        public string patient_alias { get; set; }

        [StringLength(255)]
        public string race { get; set; }

        [StringLength(255)]
        public string patient_address { get; set; }

        [StringLength(255)]
        public string country_code { get; set; }

        [StringLength(255)]
        public string phone_number_home { get; set; }

        [StringLength(255)]
        public string phone_number_business { get; set; }

        [StringLength(255)]
        public string primary_language { get; set; }

        [StringLength(255)]
        public string marital_status { get; set; }

        [StringLength(255)]
        public string religion { get; set; }

        [StringLength(255)]
        public string patient_account_number { get; set; }

        [StringLength(255)]
        public string ssn_number_patient { get; set; }

        [StringLength(255)]
        public string drivers_license_number_patient { get; set; }

        [StringLength(255)]
        public string mothers_identifier { get; set; }

        [StringLength(255)]
        public string ethnic_group { get; set; }

        [StringLength(255)]
        public string birth_place { get; set; }

        [StringLength(255)]
        public string multiple_birth_indicator { get; set; }

        [StringLength(255)]
        public string birth_order { get; set; }

        [StringLength(255)]
        public string citizenship { get; set; }

        [StringLength(255)]
        public string veterans_military_status { get; set; }

        [StringLength(255)]
        public string nationality { get; set; }

        [StringLength(255)]
        public string patient_death_date_time { get; set; }

        [StringLength(255)]
        public string identity_unknown_indicator { get; set; }

        [StringLength(255)]
        public string identity_reliability_iindicator { get; set; }

        [StringLength(255)]
        public string last_update_date_time { get; set; }

        [StringLength(255)]
        public string last_update_facility { get; set; }

        [StringLength(255)]
        public string species_code { get; set; }

        [StringLength(255)]
        public string breed_code { get; set; }

        [StringLength(255)]
        public string strain { get; set; }

        [StringLength(255)]
        public string production_class_code { get; set; }

        [StringLength(255)]
        public string tribal_citizenship { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }
    }
}
