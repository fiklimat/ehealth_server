namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pv1")]
    public partial class pv1
    {
        /// <summary>
        /// model tabulky PV1
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pv1()
        {
            message = new HashSet<message>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string set_id_pv1 { get; set; }

        [Required]
        [StringLength(255)]
        public string patient_class { get; set; }

        [StringLength(255)]
        public string assigned_patient_location { get; set; }

        [StringLength(255)]
        public string admission_type { get; set; }

        [StringLength(255)]
        public string preadmit_number { get; set; }

        [StringLength(255)]
        public string prior_patient_location { get; set; }

        [StringLength(255)]
        public string attending_doctor { get; set; }

        [StringLength(255)]
        public string referring_doctor { get; set; }

        [StringLength(255)]
        public string consulting_doctor { get; set; }

        [StringLength(255)]
        public string hospital_service { get; set; }

        [StringLength(255)]
        public string temporary_location { get; set; }

        [StringLength(255)]
        public string preadmit_test_indicator { get; set; }

        [StringLength(255)]
        public string readmission_indicator { get; set; }

        [StringLength(255)]
        public string admit_source { get; set; }

        [StringLength(255)]
        public string ambulatory_status { get; set; }

        [StringLength(255)]
        public string vip_indicator { get; set; }

        [StringLength(255)]
        public string admitting_doctor { get; set; }

        [StringLength(255)]
        public string patient_type { get; set; }

        [StringLength(255)]
        public string visit_number { get; set; }

        [StringLength(255)]
        public string financial_class { get; set; }

        [StringLength(255)]
        public string charge_price_indicator { get; set; }

        [StringLength(255)]
        public string courtesy_code { get; set; }

        [StringLength(255)]
        public string credit_rating { get; set; }

        [StringLength(255)]
        public string contract_code { get; set; }

        [StringLength(255)]
        public string contract_effective_date { get; set; }

        [StringLength(255)]
        public string contract_amount { get; set; }

        [StringLength(255)]
        public string contract_period { get; set; }

        [StringLength(255)]
        public string interest_code { get; set; }

        [StringLength(255)]
        public string transfer_to_bad_debt_code { get; set; }

        [StringLength(255)]
        public string transfer_to_bad_debt_date { get; set; }

        [StringLength(255)]
        public string bad_debt_agency_code { get; set; }

        [StringLength(255)]
        public string bad_debt_agency_amount { get; set; }

        [StringLength(255)]
        public string bad_debt_recovery_amout { get; set; }

        [StringLength(255)]
        public string delete_account_indicator { get; set; }

        [StringLength(255)]
        public string delete_account_date { get; set; }

        [StringLength(255)]
        public string discharge_disposition { get; set; }

        [StringLength(255)]
        public string discharged_to_location { get; set; }

        [StringLength(255)]
        public string diet_type { get; set; }

        [StringLength(255)]
        public string servicing_facility { get; set; }

        [StringLength(255)]
        public string bed_status { get; set; }

        [StringLength(255)]
        public string account_status { get; set; }

        [StringLength(255)]
        public string pending_location { get; set; }

        [StringLength(255)]
        public string prior_temporary_location { get; set; }

        [StringLength(255)]
        public string admit_date_time { get; set; }

        [StringLength(255)]
        public string discharge_date_time { get; set; }

        [StringLength(255)]
        public string current_patient_balance { get; set; }

        [StringLength(255)]
        public string total_charges { get; set; }

        [StringLength(255)]
        public string total_adjustments { get; set; }

        [StringLength(255)]
        public string total_payments { get; set; }

        [StringLength(255)]
        public string alternate_visit_id { get; set; }

        [StringLength(255)]
        public string visit_indicator { get; set; }

        [StringLength(255)]
        public string other_healthcare_provider { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }
    }
}
