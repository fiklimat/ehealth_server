namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obr")]
    public partial class obr
    {
        /// <summary>
        /// model tabulky OBR
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public obr()
        {
            message = new HashSet<message>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string set_id_obr { get; set; }

        [StringLength(255)]
        public string placer_order_number { get; set; }

        [StringLength(255)]
        public string filler_order_number { get; set; }

        [Required]
        [StringLength(255)]
        public string universal_service_identifier { get; set; }

        [StringLength(255)]
        public string priority_obr { get; set; }

        [StringLength(255)]
        public string requested_date_time { get; set; }

        [StringLength(255)]
        public string observation_date_time { get; set; }

        [StringLength(255)]
        public string observation_end_date_time { get; set; }

        [StringLength(255)]
        public string collection_volume { get; set; }

        [StringLength(255)]
        public string collector_identifier { get; set; }

        [StringLength(255)]
        public string specimen_action_code { get; set; }

        [StringLength(255)]
        public string danger_code { get; set; }

        [StringLength(255)]
        public string relevant_clinical_information { get; set; }

        [StringLength(255)]
        public string specimen_received_date_time { get; set; }

        [StringLength(255)]
        public string specimen_source { get; set; }

        [StringLength(255)]
        public string ordering_provider { get; set; }

        [StringLength(255)]
        public string order_callback_phone_number { get; set; }

        [StringLength(255)]
        public string placer_field_1 { get; set; }

        [StringLength(255)]
        public string placer_field_2 { get; set; }

        [StringLength(255)]
        public string filler_field_1 { get; set; }

        [StringLength(255)]
        public string filler_field_2 { get; set; }

        [StringLength(255)]
        public string result_rpt_satus_chng_date_time { get; set; }

        [StringLength(255)]
        public string charge_to_practice { get; set; }

        [StringLength(255)]
        public string diagnostic_serv_sect_id { get; set; }

        [StringLength(255)]
        public string result_status { get; set; }

        [StringLength(255)]
        public string parent_result { get; set; }

        [StringLength(255)]
        public string quantity_timing { get; set; }

        [StringLength(255)]
        public string result_copies_to { get; set; }

        [StringLength(255)]
        public string parent { get; set; }

        [StringLength(255)]
        public string transportation_mode { get; set; }

        [StringLength(255)]
        public string reason_for_study { get; set; }

        [StringLength(255)]
        public string principal_result_interpreter { get; set; }

        [StringLength(255)]
        public string assistant_result_interpreter { get; set; }

        [StringLength(255)]
        public string technician { get; set; }

        [StringLength(255)]
        public string transcriptionist { get; set; }

        [StringLength(255)]
        public string scheduled_date_time { get; set; }

        [StringLength(255)]
        public string number_of_sample_containers { get; set; }

        [StringLength(255)]
        public string transport_logistics_of_collected_sample { get; set; }

        [StringLength(255)]
        public string collectors_comment { get; set; }

        [StringLength(255)]
        public string transport_arrangement_responsibility { get; set; }

        [StringLength(255)]
        public string transport_arranged { get; set; }

        [StringLength(255)]
        public string escort_required { get; set; }

        [StringLength(255)]
        public string planned_patient_transport_comment { get; set; }

        [StringLength(255)]
        public string procedure_code { get; set; }

        [StringLength(255)]
        public string procedure_code_modifier { get; set; }

        [StringLength(255)]
        public string placer_supplemental_service_information { get; set; }

        [StringLength(255)]
        public string filler_supplemental_service_information { get; set; }

        [StringLength(255)]
        public string medically_necessary_duplivate_procedure_reason { get; set; }

        [StringLength(255)]
        public string result_handling { get; set; }

        [StringLength(255)]
        public string parent_universal_service_identifier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }
    }
}
