namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("orc")]
    public partial class orc
    {
        /// <summary>
        /// model tabulky ORC
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public orc()
        {
            message = new HashSet<message>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string order_controll { get; set; }

        [StringLength(255)]
        public string placer_order_number { get; set; }

        [StringLength(255)]
        public string filler_order_number { get; set; }

        [StringLength(255)]
        public string placer_group_number { get; set; }

        [StringLength(255)]
        public string order_status { get; set; }

        [StringLength(255)]
        public string response_flag { get; set; }

        [StringLength(255)]
        public string quantity_timing { get; set; }

        [StringLength(255)]
        public string parent_order { get; set; }

        [StringLength(255)]
        public string date_time_of_transaction { get; set; }

        [StringLength(255)]
        public string entered_by { get; set; }

        [StringLength(255)]
        public string verified_by { get; set; }

        [StringLength(255)]
        public string ordering_provider { get; set; }

        [StringLength(255)]
        public string enterers_location { get; set; }

        [StringLength(255)]
        public string call_back_phone_number { get; set; }

        [StringLength(255)]
        public string order_effective_date_time { get; set; }

        [StringLength(255)]
        public string order_code_reason { get; set; }

        [StringLength(255)]
        public string entering_organization { get; set; }

        [StringLength(255)]
        public string entering_device { get; set; }

        [StringLength(255)]
        public string action_by { get; set; }

        [StringLength(255)]
        public string advanced_beneficiary_notice_code { get; set; }

        [StringLength(255)]
        public string ordering_facility_name { get; set; }

        [StringLength(255)]
        public string ordering_facility_phone_number { get; set; }

        [StringLength(255)]
        public string ordering_provider_address { get; set; }

        [StringLength(255)]
        public string order_status_modifier { get; set; }

        [StringLength(255)]
        public string advanced_beneficiary_notice_override_reason { get; set; }

        [StringLength(255)]
        public string fillers_expected_availability_date_time { get; set; }

        [StringLength(255)]
        public string confidentiality_code { get; set; }

        [StringLength(255)]
        public string order_type { get; set; }

        [StringLength(255)]
        public string enterer_authorization_mode { get; set; }

        [StringLength(255)]
        public string parent_universal_service_identifier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }
    }
}
