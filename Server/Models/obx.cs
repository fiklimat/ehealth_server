namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obx")]
    public partial class obx
    {
        /// <summary>
        /// model tabulky OBX
        /// </summary>
        public int id { get; set; }

        public int msh_id { get; set; }

        [StringLength(255)]
        public string set_id_obx { get; set; }

        [StringLength(255)]
        public string value_type { get; set; }

        [Required]
        [StringLength(255)]
        public string observation_identifier { get; set; }

        [StringLength(255)]
        public string observation_sub_id { get; set; }

        [StringLength(255)]
        public string observation_value { get; set; }

        [StringLength(255)]
        public string units { get; set; }

        [StringLength(255)]
        public string references_range { get; set; }

        [StringLength(255)]
        public string abnormal_flags { get; set; }

        [StringLength(255)]
        public string probability { get; set; }

        [StringLength(255)]
        public string nature_of_abnormal_test { get; set; }

        [Required]
        [StringLength(255)]
        public string observation_result_status { get; set; }

        [StringLength(255)]
        public string effective_date_of_reference_date { get; set; }

        [StringLength(255)]
        public string user_defined_access_checks { get; set; }

        [StringLength(255)]
        public string date_time_of_the_observation { get; set; }

        [StringLength(255)]
        public string producers_id { get; set; }

        [StringLength(255)]
        public string responsible_observer { get; set; }

        [StringLength(255)]
        public string observation_method { get; set; }

        [StringLength(255)]
        public string equipment_instance_identifier { get; set; }

        [StringLength(255)]
        public string date_time_of_the_analysis { get; set; }

        [StringLength(255)]
        public string performing_organization_name { get; set; }

        [StringLength(255)]
        public string performing_organization_address { get; set; }

        [StringLength(255)]
        public string performing_organization_medical_director { get; set; }

        public virtual msh msh { get; set; }

        public virtual terms terms { get; set; }
    }
}
