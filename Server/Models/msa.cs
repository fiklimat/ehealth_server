namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msa")]
    public partial class msa
    {
        /// <summary>
        /// model tabulky MSA
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public msa()
        {
            message = new HashSet<message>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string acknowledgment_code { get; set; }

        [Required]
        [StringLength(255)]
        public string message_control_id { get; set; }

        [StringLength(255)]
        public string text_message { get; set; }

        [StringLength(255)]
        public string expected_sequence_number { get; set; }

        [StringLength(255)]
        public string delayed_acknowledgment_type { get; set; }

        [StringLength(255)]
        public string error_condition { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }
    }
}
