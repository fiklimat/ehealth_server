namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msh")]
    public partial class msh
    {
        /// <summary>
        /// model tabulky MSH
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public msh()
        {
            message = new HashSet<message>();
            message1 = new HashSet<message>();
            obx = new HashSet<obx>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string sending_application { get; set; }

        [StringLength(255)]
        public string sending_facility { get; set; }

        [StringLength(255)]
        public string receiving_application { get; set; }

        [StringLength(255)]
        public string receiving_facility { get; set; }

        [StringLength(255)]
        public string date_time { get; set; }

        [StringLength(255)]
        public string security { get; set; }

        [Required]
        [StringLength(255)]
        public string message_type { get; set; }

        [Required]
        [StringLength(255)]
        public string message_control { get; set; }

        [Required]
        [StringLength(255)]
        public string processing_id { get; set; }

        [StringLength(255)]
        public string version_id { get; set; }

        [StringLength(255)]
        public string sequence_number { get; set; }

        [StringLength(255)]
        public string continuation_number { get; set; }

        [StringLength(255)]
        public string accept_acknowledgment_type { get; set; }

        [StringLength(255)]
        public string applicatio_acknowledgment_type { get; set; }

        [StringLength(255)]
        public string country_code { get; set; }

        [StringLength(255)]
        public string character_set { get; set; }

        [StringLength(255)]
        public string principal_language_of_message { get; set; }

        [StringLength(255)]
        public string alternate_character_set_handling_scheme { get; set; }

        [StringLength(255)]
        public string message_profile_identifier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<message> message1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<obx> obx { get; set; }
    }
}
