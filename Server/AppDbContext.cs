using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Server
{
    public partial class AppDbContext : DbContext
    {
        /// <summary>
        /// t��da pro sv�z�n� serveru s datab�z�
        /// </summary>
        public AppDbContext()
            : base("name=AppDbContext1")
        {
        }

        public virtual DbSet<message> message { get; set; }
        public virtual DbSet<msa> msa { get; set; }
        public virtual DbSet<msh> msh { get; set; }
        public virtual DbSet<obr> obr { get; set; }
        public virtual DbSet<obx> obx { get; set; }
        public virtual DbSet<orc> orc { get; set; }
        public virtual DbSet<pid> pid { get; set; }
        public virtual DbSet<pv1> pv1 { get; set; }
        public virtual DbSet<terms> terms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<msa>()
                .Property(e => e.acknowledgment_code)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .Property(e => e.message_control_id)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .Property(e => e.text_message)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .Property(e => e.expected_sequence_number)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .Property(e => e.delayed_acknowledgment_type)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .Property(e => e.error_condition)
                .IsUnicode(false);

            modelBuilder.Entity<msa>()
                .HasMany(e => e.message)
                .WithOptional(e => e.msa)
                .HasForeignKey(e => e.msa_id);

            modelBuilder.Entity<msh>()
                .Property(e => e.sending_application)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.sending_facility)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.receiving_application)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.receiving_facility)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.date_time)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.security)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.message_type)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.message_control)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.processing_id)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.version_id)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.sequence_number)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.continuation_number)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.accept_acknowledgment_type)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.applicatio_acknowledgment_type)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.country_code)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.character_set)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.principal_language_of_message)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.alternate_character_set_handling_scheme)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .Property(e => e.message_profile_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<msh>()
                .HasMany(e => e.message)
                .WithOptional(e => e.msh)
                .HasForeignKey(e => e.msh1_id);

            modelBuilder.Entity<msh>()
                .HasMany(e => e.message1)
                .WithOptional(e => e.msh1)
                .HasForeignKey(e => e.msh2_id);

            modelBuilder.Entity<msh>()
                .HasMany(e => e.obx)
                .WithRequired(e => e.msh)
                .HasForeignKey(e => e.msh_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.set_id_obr)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.placer_order_number)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.filler_order_number)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.universal_service_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.priority_obr)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.requested_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.observation_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.observation_end_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.collection_volume)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.collector_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.specimen_action_code)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.danger_code)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.relevant_clinical_information)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.specimen_received_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.specimen_source)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.ordering_provider)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.order_callback_phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.placer_field_1)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.placer_field_2)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.filler_field_1)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.filler_field_2)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.result_rpt_satus_chng_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.charge_to_practice)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.diagnostic_serv_sect_id)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.result_status)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.parent_result)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.quantity_timing)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.result_copies_to)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.parent)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.transportation_mode)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.reason_for_study)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.principal_result_interpreter)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.assistant_result_interpreter)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.technician)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.transcriptionist)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.scheduled_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.number_of_sample_containers)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.transport_logistics_of_collected_sample)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.collectors_comment)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.transport_arrangement_responsibility)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.transport_arranged)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.escort_required)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.planned_patient_transport_comment)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.procedure_code)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.procedure_code_modifier)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.placer_supplemental_service_information)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.filler_supplemental_service_information)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.medically_necessary_duplivate_procedure_reason)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.result_handling)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .Property(e => e.parent_universal_service_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<obr>()
                .HasMany(e => e.message)
                .WithOptional(e => e.obr)
                .HasForeignKey(e => e.obr_id);

            modelBuilder.Entity<obx>()
                .Property(e => e.set_id_obx)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.value_type)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.observation_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.observation_sub_id)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.observation_value)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.units)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.references_range)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.abnormal_flags)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.probability)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.nature_of_abnormal_test)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.observation_result_status)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.effective_date_of_reference_date)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.user_defined_access_checks)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.date_time_of_the_observation)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.producers_id)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.responsible_observer)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.observation_method)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.equipment_instance_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.date_time_of_the_analysis)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.performing_organization_name)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.performing_organization_address)
                .IsUnicode(false);

            modelBuilder.Entity<obx>()
                .Property(e => e.performing_organization_medical_director)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_controll)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.placer_order_number)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.filler_order_number)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.placer_group_number)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_status)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.response_flag)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.quantity_timing)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.parent_order)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.date_time_of_transaction)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.entered_by)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.verified_by)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.ordering_provider)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.enterers_location)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.call_back_phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_effective_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_code_reason)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.entering_organization)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.entering_device)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.action_by)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.advanced_beneficiary_notice_code)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.ordering_facility_name)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.ordering_facility_phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.ordering_provider_address)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_status_modifier)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.advanced_beneficiary_notice_override_reason)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.fillers_expected_availability_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.confidentiality_code)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.order_type)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.enterer_authorization_mode)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .Property(e => e.parent_universal_service_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<orc>()
                .HasMany(e => e.message)
                .WithOptional(e => e.orc)
                .HasForeignKey(e => e.orc_id);

            modelBuilder.Entity<pid>()
                .Property(e => e.set_id_pid)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_id)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_identifier_list)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.alternate_patient_id_pid)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_name)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.mothers_maiden_name)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.date_time_birth)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.administrative_sex)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_alias)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.race)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_address)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.country_code)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.phone_number_home)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.phone_number_business)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.primary_language)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.marital_status)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.religion)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_account_number)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.ssn_number_patient)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.drivers_license_number_patient)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.mothers_identifier)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.ethnic_group)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.birth_place)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.multiple_birth_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.birth_order)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.veterans_military_status)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.nationality)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.patient_death_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.identity_unknown_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.identity_reliability_iindicator)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.last_update_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.last_update_facility)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.species_code)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.breed_code)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.strain)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.production_class_code)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.tribal_citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .HasMany(e => e.message)
                .WithOptional(e => e.pid)
                .HasForeignKey(e => e.pid_id);

            modelBuilder.Entity<pv1>()
                .Property(e => e.set_id_pv1)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.patient_class)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.assigned_patient_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.admission_type)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.preadmit_number)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.prior_patient_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.attending_doctor)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.referring_doctor)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.consulting_doctor)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.hospital_service)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.temporary_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.preadmit_test_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.readmission_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.admit_source)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.ambulatory_status)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.vip_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.admitting_doctor)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.patient_type)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.visit_number)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.financial_class)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.charge_price_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.courtesy_code)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.credit_rating)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.contract_code)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.contract_effective_date)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.contract_amount)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.contract_period)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.interest_code)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.transfer_to_bad_debt_code)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.transfer_to_bad_debt_date)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.bad_debt_agency_code)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.bad_debt_agency_amount)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.bad_debt_recovery_amout)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.delete_account_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.delete_account_date)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.discharge_disposition)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.discharged_to_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.diet_type)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.servicing_facility)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.bed_status)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.account_status)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.pending_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.prior_temporary_location)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.admit_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.discharge_date_time)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.current_patient_balance)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.total_charges)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.total_adjustments)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.total_payments)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.alternate_visit_id)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.visit_indicator)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .Property(e => e.other_healthcare_provider)
                .IsUnicode(false);

            modelBuilder.Entity<pv1>()
                .HasMany(e => e.message)
                .WithOptional(e => e.pv1)
                .HasForeignKey(e => e.pv1_id);

            modelBuilder.Entity<terms>()
                .Property(e => e.original)
                .IsUnicode(false);

            modelBuilder.Entity<terms>()
                .Property(e => e.snomed)
                .IsUnicode(false);

            modelBuilder.Entity<terms>()
                .Property(e => e.loinc)
                .IsUnicode(false);

            modelBuilder.Entity<terms>()
                .HasMany(e => e.obx)
                .WithRequired(e => e.terms)
                .HasForeignKey(e => e.observation_identifier)
                .WillCascadeOnDelete(false);
        }
    }
}
