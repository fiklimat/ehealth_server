﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class PIDService : IPIDService
    {
        /// <summary>
        /// Service pro práci s tabulkou PID
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public pid Create(string line)
        {
            using (var db = new AppDbContext())
            {
                pid pid = new pid();
                string[] arr = line.Split('|');
                pid.set_id_pid = arr[1];
                pid.patient_id = arr[2];
                pid.patient_identifier_list = arr[3];
                pid.alternate_patient_id_pid = arr[4];
                pid.patient_name = arr[5];
                pid.mothers_maiden_name = arr[6];
                pid.date_time_birth = arr[7];
                pid.administrative_sex = arr[8];
                db.pid.Add(pid);
                db.SaveChanges();
                return pid;
            }
        }

        // metoda pro získání všech pacientských id
        public List<string> GetIds()
        {
            using (var db = new AppDbContext())
            {
                List<string> allIds = new List<string>();
                foreach (pid pid in db.pid)
                {
                    if (!allIds.Contains(pid.patient_identifier_list))
                        allIds.Add(pid.patient_identifier_list);
                }
                return allIds;
            }
        }
    }
}