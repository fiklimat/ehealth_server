﻿using Server.Controllers;
using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class MessageService : IMessageService
    {
        /// <summary>
        /// Service pro práci s tabulkou message
        /// </summary>
         
        // metoda pro vytvoření nového záznamu v databázi
        public message Create(message message_new)
        {
            using (var db = new AppDbContext())
            {
                db.message.Add(message_new);
                db.SaveChanges();
                return message_new;
            }
        }

        // metoda pro vytvoření Observation zpráv na základě přijaté zprávy Diagnostic Report
        public List<Observation> GetHL7FHIRMessage(DiagnosticReport diagnosticReport)
        {
            using (var db = new AppDbContext())
            {
                // vytažení potřebných údajů z Diagnostic Report
                string patient_id = diagnosticReport.subject.identifier[0].value;
                long start_time_int = Int64.Parse(diagnosticReport.effectivePeriod.start);
                long end_time_int = Int64.Parse(diagnosticReport.effectivePeriod.end);

                //list obsahující id všech záznamů z tabulky pid pro zadaného pacienta
                List<int> pid_ids = (from pd in db.pid where pd.patient_identifier_list == patient_id select pd.id).ToList();

                // vytvoření listů pro ukládání údajů z databáze
                List<int> msh_ids = new List<int>();
                List<obx> obx_filtrated_entries = new List<obx>();
                string terminology = "";

                // nalezení všech záznamů MSH pro daného pacienta podle záznamů PID
                foreach (int id in pid_ids)
                {
                    int msh_id = (from msg in db.message where msg.pid_id == id select msg.msh1_id).FirstOrDefault().Value;
                    msh_ids.Add(msh_id);
                }

                // nalezení odpovídajících OBX záznamů pro zadané termíny podle MSH záznamů
                foreach (var entry in diagnosticReport.code.coding)
                {

                    List<obx> obx_entries = new List<obx>();
                    terminology = entry.system;
                    string term = entry.code + '^' + entry.display;
                    foreach (int msh_id in msh_ids)
                    {
                        obx obx = new obx();
                        if (terminology == "Original")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.original == term select ob).FirstOrDefault();
                        else if (terminology == "SNOMED")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.snomed == term select ob).FirstOrDefault();
                        else if (terminology == "LOINC")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.loinc == term select ob).FirstOrDefault();
                        obx_entries.Add(obx);
                    }

                    // filtrace těch OBX záznamů, které odpovídají zadanému času
                    foreach (obx obx in obx_entries)
                    {
                        if (obx != default)
                            if (Int64.Parse(obx.date_time_of_the_observation) >= start_time_int && Int64.Parse(obx.date_time_of_the_observation) <= end_time_int)
                                obx_filtrated_entries.Add(obx);
                    }
                }

                // vytvoření Observation zpráv ze získaných dat
                List<Observation> observations = new List<Observation>();
                foreach (obx obx in obx_filtrated_entries)
                {
                    string[] temp = new string[2];
                    if (terminology == "Original")
                    {
                        temp = obx.observation_identifier.Split('^');
                    }
                    else if (terminology == "SNOMED")
                    {
                        temp = obx.terms.snomed.Split('^');
                    }
                    else if (terminology == "LOINC")
                    {
                        temp = obx.terms.loinc.Split('^');
                    }
                    Coding coding = new Coding() 
                    {
                        code = temp[0],
                        display = temp[1],
                        system = terminology
                    };
                    Coding[] codings = new Coding[1];
                    codings[0] = coding;
                    Code code = new Code() 
                    {
                        coding = codings
                    };
                    Identifier[] identifiers = new Identifier[1];
                    Identifier identifier = new Identifier() 
                    {
                        value = diagnosticReport.subject.identifier[0].value
                    };
                    identifiers[0] = identifier;
                    Subject subject = new Subject()
                    {
                        resourceType = "Patient",
                        identifier = identifiers
                    };
                    ValueQuantity valueQuantity = new ValueQuantity()
                    {
                        value = obx.observation_value,
                        unit = obx.units
                    };
                    Observation observation = new Observation {
                        resourceType = "Observation",
                        identifier = new Identifier { 
                            value = DateTime.Now.ToString("yyyyMMddHHmmss")
                        },
                        status = "final",
                        code = code,
                        subject = subject,
                        effectiveDateTime = obx.date_time_of_the_observation,
                        valueQuantity = valueQuantity
                    };
                    observations.Add(observation);
                }
                return observations;
            }
        }

        // metoda pro vytvoření HL7 zprávy na základě parametrů z přijaté ORM zprávy
        public string GetHL7Message(string patient_identifier_list, string[] terms, string start_time, string end_time)
        {
            using (var db = new AppDbContext())
            {
                long start_time_int = Int64.Parse(start_time);
                long end_time_int = Int64.Parse(end_time);

                //list obsahující id všech záznamů z tabulky pid pro zadaného pacienta
                List<int> pid_ids = (from pd in db.pid where pd.patient_identifier_list == patient_identifier_list select pd.id).ToList();

                // vytvoření listů pro ukládání údajů z databáze
                List<int> msh_ids = new List<int>();
                List<obx> obx_filtrated_entries = new List<obx>();
                string terminology = "";

                // nalezení všech záznamů MSH pro daného pacienta podle záznamů PID
                foreach (int id in pid_ids)
                {
                    int msh_id = (from msg in db.message where msg.pid_id == id select msg.msh1_id).FirstOrDefault().Value;
                    msh_ids.Add(msh_id);
                }

                // nalezení odpovídajících OBX záznamů pro zadané termíny podle MSH záznamů
                foreach (string entry in terms)
                {

                    List<obx> obx_entries = new List<obx>();
                    string[] temp = entry.Split('^');
                    terminology = temp[2];
                    string term = temp[0] + '^' + temp[1];
                    foreach (int msh_id in msh_ids)
                    {
                        obx obx = new obx();
                        if (terminology == "Original")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.original == term select ob).FirstOrDefault();
                        else if (terminology == "SNOMED")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.snomed == term select ob).FirstOrDefault();
                        else if (terminology == "LOINC")
                            obx = (from ob in db.obx where ob.msh_id == msh_id && ob.terms.loinc == term select ob).FirstOrDefault();
                        obx_entries.Add(obx);
                    }

                    // filtrace těch OBX záznamů, které odpovídají zadanému času
                    foreach (obx obx in obx_entries)
                    {
                        if (obx != default)
                            if (Int64.Parse(obx.date_time_of_the_observation) >= start_time_int && Int64.Parse(obx.date_time_of_the_observation) <= end_time_int)
                                obx_filtrated_entries.Add(obx);
                    }
                }

                // vytvoření HL7 zprávy ze získaných dat
                string date_time = DateTime.Now.ToString("yyyyMMddHHmmss");
                string message_id = date_time;
                string HL7_message = "MSH|^~\\&|SERVER APP|SERVER APP|CLIENT APP|CLIENT APP|" + date_time + "||ORU^R01^ORU_R0|" + message_id + "|P|2.4|||NE|AL|CZE|ASCII||ASCII\n" +
                    "PID|||" + patient_identifier_list + "||^^^^^^L^A|||O\n" +
                    "PV1||I|^^OR-1^10.2.56.5:1\n" +
                    "ORC|RE\n" +
                    "OBR|1|||VITAL|||" + start_time + "|" + end_time + "|||||||||||||||||A\n";
                int i = 1;
                if (terminology == "Original")
                    foreach (obx obx in obx_filtrated_entries)
                    {
                        HL7_message += "OBX|" + i.ToString() + "|" + obx.value_type + "|" + obx.observation_identifier  + "^Original|1|" + obx.observation_value + "|" + obx.units + "|||||F|||" + obx.date_time_of_the_observation + "|||\n";
                        i++;
                    }
                else if (terminology == "SNOMED")
                    foreach (obx obx in obx_filtrated_entries)
                    {
                        HL7_message += "OBX|" + i.ToString() + "|" + obx.value_type + "|" + obx.terms.snomed + "^SNOMED|1|" + obx.observation_value + "|" + obx.units + "|||||F|||" + obx.date_time_of_the_observation + "|||\n";
                        i++;
                    }
                else if (terminology == "LOINC")
                    foreach (obx obx in obx_filtrated_entries)
                    {
                        HL7_message += "OBX|" + i.ToString() + "|" + obx.value_type + "|" + obx.terms.loinc + "^LOINC|1|" + obx.observation_value + "|" + obx.units + "|||||F|||" + obx.date_time_of_the_observation + "|||\n";
                        i++;
                    }
                return HL7_message;
            }
        }
    }
}