﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class OBRService : IOBRService
    {
        /// <summary>
        /// Service pro práci s tabulkou OBR
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public obr Create(string line)
        {
            using (var db = new AppDbContext())
            {
                obr obr = new obr();
                string[] arr = line.Split('|');
                obr.set_id_obr = arr[1];
                obr.placer_order_number = arr[2];
                obr.filler_order_number = arr[3];
                obr.universal_service_identifier = arr[4];
                obr.priority_obr = arr[5];
                obr.requested_date_time = arr[6];
                obr.observation_date_time = arr[7];
                obr.observation_end_date_time = arr[8];
                obr.collection_volume = arr[9];
                obr.collector_identifier = arr[10];
                obr.specimen_action_code = arr[11];
                obr.danger_code = arr[12];
                obr.relevant_clinical_information = arr[13];
                obr.specimen_received_date_time = arr[14];
                obr.specimen_source = arr[15];
                obr.ordering_provider = arr[16];
                obr.order_callback_phone_number = arr[17];
                obr.placer_field_1 = arr[18];
                obr.placer_field_2 = arr[19];
                obr.filler_field_1 = arr[20];
                obr.filler_field_2 = arr[21];
                obr.result_rpt_satus_chng_date_time = arr[22];
                obr.charge_to_practice = arr[23];
                obr.diagnostic_serv_sect_id = arr[24];
                obr.result_status = arr[25];
                db.obr.Add(obr);
                db.SaveChanges();
                return obr;
            }
        }

        // metoda pro získání všech časů, ve kterých proběhlo měření u daného pacienta
        public List<string> GetTimesForPatient(string id)
        {
            using (var db = new AppDbContext())
            {
                List<string> times = new List<string>();
                List<obr> obrs = db.obr.ToList<obr>();
                foreach (obr obr in obrs)
                {
                    message message = db.message.Where(msg => msg.obr_id == obr.id).FirstOrDefault();
                    pid pid = db.pid.Where(p => p.id == message.pid_id).FirstOrDefault();
                    if (pid.patient_identifier_list == id && !times.Contains(obr.observation_date_time))
                        times.Add(obr.observation_date_time);
                }
                return times;
            }
        }
    }
}