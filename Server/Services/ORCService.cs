﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class ORCService : IORCService
    {
        /// <summary>
        /// Service pro práci s tabulkou ORC
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public orc Create(string line)
        {
            using (var db = new AppDbContext())
            {
                orc orc = new orc();
                string[] arr = line.Split('|');
                orc.order_controll = arr[1];
                db.orc.Add(orc);
                db.SaveChanges();
                return orc;
            }
        }
    }
}