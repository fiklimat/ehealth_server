﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class MSHService : IMSHService
    {
        /// <summary>
        /// Service pro práci s tabulkou MSH
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public msh Create(string line)
        {
            using (var db = new AppDbContext())
            {
                msh msh = new msh();
                string[] arr = line.Split('|');
                msh.sending_application = arr[2];
                msh.sending_facility = arr[3];
                msh.receiving_application = arr[4];
                msh.receiving_facility = arr[5];
                msh.date_time = arr[6];
                msh.security = arr[7];
                msh.message_type = arr[8];
                msh.message_control = arr[9];
                msh.processing_id = arr[10];
                msh.version_id = arr[11];
                msh.sequence_number = arr[12];
                msh.continuation_number = arr[13];
                msh.accept_acknowledgment_type = arr[14];
                msh.applicatio_acknowledgment_type = arr[15];
                msh.country_code = arr[16];
                msh.character_set = arr[17];
                msh.principal_language_of_message = arr[18];
                msh.alternate_character_set_handling_scheme = arr[19];
                db.msh.Add(msh);
                db.SaveChanges();
                return msh;
            }
        }
    }
}