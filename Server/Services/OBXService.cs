﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class OBXService : IOBXService
    {
        /// <summary>
        /// Service pro práci s tabulkou OBX
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public obx Create(string line, int msh_id)
        {
            using (var db = new AppDbContext())
            {
                obx obx = new obx();
                string[] arr = line.Split('|');
                string original = arr[3];
                terms term = (from tm in db.terms where tm.original == original select tm).FirstOrDefault();
                if (term == default)
                {
                    term = new terms();
                    term.original = arr[3];
                    db.terms.Add(term);
                    db.SaveChanges();
                }
                obx.msh_id = msh_id;
                obx.set_id_obx = arr[1];
                obx.value_type = arr[2];
                obx.observation_identifier = term.original;
                obx.observation_sub_id = arr[4];
                obx.observation_value = arr[5];
                obx.units = arr[6];
                obx.references_range = arr[7];
                obx.abnormal_flags = arr[8];
                obx.probability = arr[9];
                obx.nature_of_abnormal_test = arr[10];
                obx.observation_result_status = arr[11];
                obx.effective_date_of_reference_date = arr[12];
                obx.user_defined_access_checks = arr[13];
                obx.date_time_of_the_observation = arr[14];
                obx.producers_id = arr[15];
                obx.responsible_observer = arr[16];
                db.obx.Add(obx);
                db.SaveChanges();
                return obx;
            }
        }
    }
}