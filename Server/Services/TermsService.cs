﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class TermsService : ITermsService
    {
        /// <summary>
        /// Service pro práci s tabulkou terms
        /// </summary>

        // metoda pro získání všech záznamů z tabulky
        public List<terms> GetAll()
        {
            using (var db = new AppDbContext())
            {
                List <terms>terms = (from tm in db.terms select tm).ToList();
                return terms;
            }
        }

        // metoda pro doplnění snomed a loinc termínů k odpovídajícím originálním termínům
        public bool UpdateTerminology(string[] lines)
        {
            using (var db = new AppDbContext())
            {
                foreach (string line in lines)
                {
                    string[] cols = line.Split('|');
                    string original = cols[0];
                    terms term = (from te in db.terms where te.original == original select te).FirstOrDefault();
                    db.terms.Attach(term);
                    term.snomed = cols[1];
                    term.loinc = cols[2];
                    db.SaveChanges();
                }
            }
            return true;
        }
    }
}