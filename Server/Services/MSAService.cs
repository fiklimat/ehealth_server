﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class MSAService : IMSAService
    {
        /// <summary>
        /// Service pro práci s tabulkou MSA
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public msa Create(string line)
        {
            using (var db = new AppDbContext())
            {
                msa msa = new msa();
                string[] arr = line.Split('|');
                msa.acknowledgment_code = arr[1];
                msa.message_control_id = arr[2];
                db.msa.Add(msa);
                db.SaveChanges();
                return msa;
            }
        }
    }
}