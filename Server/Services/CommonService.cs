﻿using Server.Core;
using Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Server.Services
{
    public class CommonService : ICommonService
    {
        /// <summary>
        /// Service pro tvoření HTTP odpovědí
        /// </summary>
        
        // metoda pro vytvoření odpovědi pouze se stavovým kódem
        public HttpResponseMessage GetResponse(int statusCode)
        {
            var result = new HttpResponseMessage();
            result.ApplyStatusCode(statusCode);

            return result;
        }

        // metoda pro vytvoření odpovědi pouze s daty ve formátu string
        public HttpResponseMessage GetResponse(string content)
        {
            var result = GetBaseResponse(content);
            result.ApplyTxtContentType();
            return result;
        }

        // metoda pro vytvoření odpovědi s daty i stavovým kódem
        public HttpResponseMessage GetResponse(string content, int statusCode)
        {
            var result = GetBaseResponse(content);
            result.ApplyTxtContentType();
            result.ApplyStatusCode(statusCode);
            return result;
        }

        // metoda pro vytvoření HTML odpovědi s danými daty ve formátu string
        private HttpResponseMessage GetBaseResponse(string content)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(content)
            };
        }
    }
}