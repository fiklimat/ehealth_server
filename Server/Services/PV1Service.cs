﻿using Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Services
{
    public class PV1Service : IPV1Service
    {
        /// <summary>
        /// Service pro práci s tabulkou PV1
        /// </summary>

        // metoda pro vytvoření nového záznamu v databázi
        public pv1 Create(string line)
        {
            using (var db = new AppDbContext())
            {
                pv1 pv1 = new pv1();
                string[] arr = line.Split('|');
                pv1.set_id_pv1 = arr[1];
                pv1.patient_class = arr[2];
                pv1.assigned_patient_location = arr[3];
                db.pv1.Add(pv1);
                db.SaveChanges();
                return pv1;
            }
        }
    }
}