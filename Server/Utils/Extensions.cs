﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Server.Utils
{
    public static class Extensions
    {
        /// <summary>
        /// třída s doplňujícími metodami
        /// </summary>
        
        // metoda upřesňující typ dat v HTML zprávě jako text/plain
        public static void ApplyTxtContentType(this HttpResponseMessage msg, string mediaType = "text/plain")
        {
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(mediaType);
        }

        // metoda připojující k HTML zprávě stavový kód
        public static void ApplyStatusCode(this HttpResponseMessage msg, int statusCode)
        {
            msg.StatusCode = (System.Net.HttpStatusCode)statusCode;
        }

        // metoda pro převedení Observation zpráv na JSON
        public static JArray observationsToJson(this List<Controllers.Observation> observations) 
        {
            JArray arr = new JArray();
            foreach (Controllers.Observation observation in observations)
            {
                arr.Add(observation.observationToJson());
            }
            return arr;
        }

        // metoda pro převedení jedné Observation zprávy na JSON
        public static JObject observationToJson(this Controllers.Observation observation) 
        {
            dynamic obj = new JObject();
            obj.resourceType = observation.resourceType;
            dynamic identifier = new JObject();
            identifier.value = observation.identifier.value;
            obj.identifier = identifier;
            obj.status = observation.status;
            dynamic code = new JObject();
            JArray coding = new JArray();
            dynamic coding_0 = new JObject();
            coding_0.system = observation.code.coding[0].system;
            coding_0.code = observation.code.coding[0].code;
            coding_0.display = observation.code.coding[0].display;
            coding.Add(coding_0);
            code.coding = coding;
            obj.code = code;
            dynamic identifier_0 = new JObject();
            identifier_0.value = observation.subject.identifier[0].value;
            JArray ident = new JArray();
            ident.Add(identifier_0);
            dynamic subject = new JObject();
            subject.identifier = ident;
            subject.resourceType = observation.subject.resourceType;
            obj.subject = subject;
            obj.effectiveDateTime = observation.effectiveDateTime;
            dynamic valueQuantity = new JObject();
            valueQuantity.value = observation.valueQuantity.value;
            valueQuantity.unit = observation.valueQuantity.unit;
            obj.valueQuantity = valueQuantity;
            return obj;
        }

        // metoda pro převedení termínů na string
        public static string termsToString(this List<terms> terms)
        {
            string message = "";
            foreach (terms term in terms)
            {
                message += term.original + "|" + term.snomed + "|" + term.loinc + "\n";
            }
            return message;
        }

        // metoda pro převedení listu na string
        public static string listToString(this List<string> list)
        {
            string message = "";
            foreach (string item in list)
            {
                message += item + "\n";
            }
            return message;
        }
        
        // metoda pro logování HL7 zprávy
        public static void logMessageHL7(this string message)
        {
            using (StreamWriter sw = File.AppendText(@"D:\škola\7 semestr\ehealth\ukol\logHL7.txt"))
            {
                sw.Write(message);
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        // metoda pro logování HL7 FHIR zprávy
        public static void logMessageHL7FHIR(this string message)
        {
            using (StreamWriter sw = File.AppendText(@"D:\škola\7 semestr\ehealth\ukol\logHL7FHIR.txt"))
            {
                sw.Write(message);
                sw.WriteLine();
                sw.WriteLine();
            }
        }
    }
}