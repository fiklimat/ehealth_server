clear; close; clc;

messages = dir('original/*.txt');
N = length(messages);
important = false; % pro oddělení podstatných a nepodstatných řádků

one_patient = true; % pro ověření shodného pacienta v rámci souboru
consistent_time = true; % pro ověření shodného času v rámci zprávy
consistent_message = true; % pro ověření počátku a konce zprávy

% pole pro ukládání konkrétních chyb při ověřování
deviations = strings(1000,5);
deviations(1, :) = ["file" "line" "type" "first value" "actual value"];
deviations_index = 2;

% id a parametr pro vykreslení
patient_id = "2011032";
parameter = "001000^VITAL HR";

% pole pro ukládání dat pro vykreslení
values = zeros(10000,1);
date_times = strings(10000,1);
index = 1;

for i = 1:N 
    PID = "";
    message = readlines("original/" + messages(i).name);
    for j = 1 : size(message,1)
        % pokud se očekává počátek, ale další řádek obsahuje MSA, pak
        % počátek zprávy chybí
        if contains(message(j), "MSH") && ~important
            if contains(message(j+1), "MSA")
                consistent_message = false;
                deviations(deviations_index, 1) = messages(i).name;
                deviations(deviations_index, 2) = j;
                deviations(deviations_index, 3) = "first header missing";
                deviations_index = deviations_index + 1;
            else %nalezen počátek zprávy
                important = true;
                start_index = j;
            end
        % pokud se očekává konec, ale další řádek obsahuje PID, pak
        % konec zprávy chybí
        elseif contains(message(j), "MSH") && important
            if contains(message(j+1), "PID")
                consistent_message = false;
                deviations(deviations_index, 1) = string(messages(i).name);
                deviations(deviations_index, 2) = j;
                deviations(deviations_index, 3) = "last header missing";
                deviations_index = deviations_index + 1;
                start_index = j;
            else %nalezen konec zprávy
                important = false;
                % zapsání celé zprávy do souboru
                for k = start_index : j+2
                    fprintf(fid, replace(message(k), '\&', '\\&') + '\n');
                end
                fclose(fid);
            end 
        end
        if contains(message(j), "PID")
            line_arr = split(message(j), '|');
            % pokud se jedná o první zprávu v souboru, uloží se id pacienta
            % pro ověřování
            if PID == ""
                PID = line_arr(4);
            % ověření jestli se jedná o stejného pacienta
            elseif line_arr(4) ~= PID
                one_patient = false;
                deviations(deviations_index, 1) = messages(i).name;
                deviations(deviations_index, 2) = j;
                deviations(deviations_index, 3) = "another patient";
                deviations(deviations_index, 4) = PID;
                deviations(deviations_index, 5) = line_arr(4);
                deviations_index = deviations_index + 1;
            end
            % vytvoření nového souboru podle id pacienta
            file_name = "by_id/comm_" + line_arr(4) + ".txt";
            fid = fopen(file_name, 'at');
        end
        % ověření, jestli čas všech OBX záznamů v rámci jedné zprávy
        % odpovídá času nadřazeného OBR
        if contains(message(j), "OBR")
            line_arr = split(message(j), '|');
            date_time = line_arr(8);
        end
        if contains(message(j), "OBX")
            line_arr = split(message(j), '|');
            if line_arr(15) ~= date_time
                consistent_time = false;
                deviations(deviations_index, 1) = messages(i).name;
                deviations(deviations_index, 2) = j;
                deviations(deviations_index, 3) = "another timestamp";
                deviations(deviations_index, 4) = date_time;
                deviations(deviations_index, 5) = line_arr(15);
                deviations_index = deviations_index + 1;
            end
        end
    end
end

% načtení vybraného parametru z odpovídajícího souboru
message = readlines("by_id/comm_" + patient_id + ".txt");
for i = 1 : size(message)
    if contains(message(i), "OBX") && contains(message(i), parameter)
        line_arr = split(message(i), '|');
        values(index) = line_arr(6);
        date = char(line_arr(15));
        if date ~= ""
            date_times(index) = datetime(date(7:8) + "." + date(5:6) + "." + date(1:4) + " " + date(9:10) + ":" + date(11:12) + ":" + date(13:14));
            index = index + 1;
        end
    end
end

% vykreslení požadovaného parametru
date_times = datetime(date_times(find(date_times~="")));
values = values(1:size(date_times));
plot(date_times, values, 'DatetimeTickFormat', 'dd-MM-yyyy HH:mm:ss')