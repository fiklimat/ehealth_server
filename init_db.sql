CREATE DATABASE telemed_database CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

use telemed_database;
CREATE TABLE `msh` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `sending_application` varchar(255),
  `sending_facility` varchar(255),
  `receiving_application` varchar(255),
  `receiving_facility` varchar(255),
  `date_time` varchar(255),
  `security` varchar(255),
  `message_type` varchar(255) NOT NULL,
  `message_control` varchar(255) NOT NULL,
  `processing_id` varchar(255) NOT NULL,
  `version_id` varchar(255),
  `sequence_number` varchar(255),
  `continuation_number` varchar(255),
  `accept_acknowledgment_type` varchar(255),
  `applicatio_acknowledgment_type` varchar(255),
  `country_code` varchar(255),
  `character_set` varchar(255),
  `principal_language_of_message` varchar(255),
  `alternate_character_set_handling_scheme` varchar(255),
  `message_profile_identifier` varchar(255)
);

CREATE TABLE `pid` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `set_id_pid` varchar(255),
  `patient_id` varchar(255),
  `patient_identifier_list` varchar(255) NOT NULL,
  `alternate_patient_id_pid` varchar(255),
  `patient_name` varchar(255) NOT NULL,
  `mothers_maiden_name` varchar(255),
  `date_time_birth` varchar(255),
  `administrative_sex` varchar(255),
  `patient_alias` varchar(255),
  `race` varchar(255),
  `patient_address` varchar(255),
  `country_code` varchar(255),
  `phone_number_home` varchar(255),
  `phone_number_business` varchar(255),
  `primary_language` varchar(255),
  `marital_status` varchar(255),
  `religion` varchar(255),
  `patient_account_number` varchar(255),
  `ssn_number_patient` varchar(255),
  `drivers_license_number_patient` varchar(255),
  `mothers_identifier` varchar(255),
  `ethnic_group` varchar(255),
  `birth_place` varchar(255),
  `multiple_birth_indicator` varchar(255),
  `birth_order` varchar(255),
  `citizenship` varchar(255),
  `veterans_military_status` varchar(255),
  `nationality` varchar(255),
  `patient_death_date_time` varchar(255),
  `identity_unknown_indicator` varchar(255),
  `identity_reliability_iindicator` varchar(255),
  `last_update_date_time` varchar(255),
  `last_update_facility` varchar(255),
  `species_code` varchar(255),
  `breed_code` varchar(255),
  `strain` varchar(255),
  `production_class_code` varchar(255),
  `tribal_citizenship` varchar(255)
);

CREATE TABLE `pv1` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `set_id_pv1` varchar(255),
  `patient_class` varchar(255) NOT NULL,
  `assigned_patient_location` varchar(255),
  `admission_type` varchar(255),
  `preadmit_number` varchar(255),
  `prior_patient_location` varchar(255),
  `attending_doctor` varchar(255),
  `referring_doctor` varchar(255),
  `consulting_doctor` varchar(255),
  `hospital_service` varchar(255),
  `temporary_location` varchar(255),
  `preadmit_test_indicator` varchar(255),
  `readmission_indicator` varchar(255),
  `admit_source` varchar(255),
  `ambulatory_status` varchar(255),
  `vip_indicator` varchar(255),
  `admitting_doctor` varchar(255),
  `patient_type` varchar(255),
  `visit_number` varchar(255),
  `financial_class` varchar(255),
  `charge_price_indicator` varchar(255),
  `courtesy_code` varchar(255),
  `credit_rating` varchar(255),
  `contract_code` varchar(255),
  `contract_effective_date` varchar(255),
  `contract_amount` varchar(255),
  `contract_period` varchar(255),
  `interest_code` varchar(255),
  `transfer_to_bad_debt_code` varchar(255),
  `transfer_to_bad_debt_date` varchar(255),
  `bad_debt_agency_code` varchar(255),
  `bad_debt_agency_amount` varchar(255),
  `bad_debt_recovery_amout` varchar(255),
  `delete_account_indicator` varchar(255),
  `delete_account_date` varchar(255),
  `discharge_disposition` varchar(255),
  `discharged_to_location` varchar(255),
  `diet_type` varchar(255),
  `servicing_facility` varchar(255),
  `bed_status` varchar(255),
  `account_status` varchar(255),
  `pending_location` varchar(255),
  `prior_temporary_location` varchar(255),
  `admit_date_time` varchar(255),
  `discharge_date_time` varchar(255),
  `current_patient_balance` varchar(255),
  `total_charges` varchar(255),
  `total_adjustments` varchar(255),
  `total_payments` varchar(255),
  `alternate_visit_id` varchar(255),
  `visit_indicator` varchar(255),
  `other_healthcare_provider` varchar(255)
);

CREATE TABLE `orc` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `order_controll` varchar(255) NOT NULL,
  `placer_order_number` varchar(255),
  `filler_order_number` varchar(255),
  `placer_group_number` varchar(255),
  `order_status` varchar(255),
  `response_flag` varchar(255),
  `quantity_timing` varchar(255),
  `parent_order` varchar(255),
  `date_time_of_transaction` varchar(255),
  `entered_by` varchar(255),
  `verified_by` varchar(255),
  `ordering_provider` varchar(255),
  `enterers_location` varchar(255),
  `call_back_phone_number` varchar(255),
  `order_effective_date_time` varchar(255),
  `order_code_reason` varchar(255),
  `entering_organization` varchar(255),
  `entering_device` varchar(255),
  `action_by` varchar(255),
  `advanced_beneficiary_notice_code` varchar(255),
  `ordering_facility_name` varchar(255),
  `ordering_facility_phone_number` varchar(255),
  `ordering_provider_address` varchar(255),
  `order_status_modifier` varchar(255),
  `advanced_beneficiary_notice_override_reason` varchar(255),
  `fillers_expected_availability_date_time` varchar(255),
  `confidentiality_code` varchar(255),
  `order_type` varchar(255),
  `enterer_authorization_mode` varchar(255),
  `parent_universal_service_identifier` varchar(255)
);

CREATE TABLE `obr` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `set_id_obr` varchar(255),
  `placer_order_number` varchar(255),
  `filler_order_number` varchar(255),
  `universal_service_identifier` varchar(255) NOT NULL,
  `priority_obr` varchar(255),
  `requested_date_time` varchar(255),
  `observation_date_time` varchar(255),
  `observation_end_date_time` varchar(255),
  `collection_volume` varchar(255),
  `collector_identifier` varchar(255),
  `specimen_action_code` varchar(255),
  `danger_code` varchar(255),
  `relevant_clinical_information` varchar(255),
  `specimen_received_date_time` varchar(255),
  `specimen_source` varchar(255),
  `ordering_provider` varchar(255),
  `order_callback_phone_number` varchar(255),
  `placer_field_1` varchar(255),
  `placer_field_2` varchar(255),
  `filler_field_1` varchar(255),
  `filler_field_2` varchar(255),
  `result_rpt_satus_chng_date_time` varchar(255),
  `charge_to_practice` varchar(255),
  `diagnostic_serv_sect_id` varchar(255),
  `result_status` varchar(255),
  `parent_result` varchar(255),
  `quantity_timing` varchar(255),
  `result_copies_to` varchar(255),
  `parent` varchar(255),
  `transportation_mode` varchar(255),
  `reason_for_study` varchar(255),
  `principal_result_interpreter` varchar(255),
  `assistant_result_interpreter` varchar(255),
  `technician` varchar(255),
  `transcriptionist` varchar(255),
  `scheduled_date_time` varchar(255),
  `number_of_sample_containers` varchar(255),
  `transport_logistics_of_collected_sample` varchar(255),
  `collectors_comment` varchar(255),
  `transport_arrangement_responsibility` varchar(255),
  `transport_arranged` varchar(255),
  `escort_required` varchar(255),
  `planned_patient_transport_comment` varchar(255),
  `procedure_code` varchar(255),
  `procedure_code_modifier` varchar(255),
  `placer_supplemental_service_information` varchar(255),
  `filler_supplemental_service_information` varchar(255),
  `medically_necessary_duplivate_procedure_reason` varchar(255),
  `result_handling` varchar(255),
  `parent_universal_service_identifier` varchar(255)
);

CREATE TABLE `terms` (
  `original` varchar(255) PRIMARY KEY NOT NULL,
  `snomed` varchar(255),
  `loinc` varchar(255)
);

CREATE TABLE `obx` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `msh_id` int NOT NULL,
  `set_id_obx` varchar(255),
  `value_type` varchar(255),
  `observation_identifier` varchar(255) NOT NULL,
  `observation_sub_id` varchar(255),
  `observation_value` varchar(255),
  `units` varchar(255),
  `references_range` varchar(255),
  `abnormal_flags` varchar(255),
  `probability` varchar(255),
  `nature_of_abnormal_test` varchar(255),
  `observation_result_status` varchar(255) NOT NULL,
  `effective_date_of_reference_date` varchar(255),
  `user_defined_access_checks` varchar(255),
  `date_time_of_the_observation` varchar(255),
  `producers_id` varchar(255),
  `responsible_observer` varchar(255),
  `observation_method` varchar(255),
  `equipment_instance_identifier` varchar(255),
  `date_time_of_the_analysis` varchar(255),
  `performing_organization_name` varchar(255),
  `performing_organization_address` varchar(255),
  `performing_organization_medical_director` varchar(255)
);

CREATE TABLE `msa` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `acknowledgment_code` varchar(255) NOT NULL,
  `message_control_id` varchar(255) NOT NULL,
  `text_message` varchar(255),
  `expected_sequence_number` varchar(255),
  `delayed_acknowledgment_type` varchar(255),
  `error_condition` varchar(255)
);

CREATE TABLE `message` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `msh1_id` int,
  `msh2_id` int,
  `pid_id` int,
  `pv1_id` int,
  `orc_id` int,
  `obr_id` int,
  `msa_id` int
);

ALTER TABLE `obx` ADD FOREIGN KEY (`msh_id`) REFERENCES `msh` (`id`);

ALTER TABLE `obx` ADD FOREIGN KEY (`observation_identifier`) REFERENCES `terms` (`original`);

ALTER TABLE `message` ADD FOREIGN KEY (`msh1_id`) REFERENCES `msh` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`msh2_id`) REFERENCES `msh` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`pid_id`) REFERENCES `pid` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`pv1_id`) REFERENCES `pv1` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`orc_id`) REFERENCES `orc` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`obr_id`) REFERENCES `obr` (`id`);

ALTER TABLE `message` ADD FOREIGN KEY (`msa_id`) REFERENCES `msa` (`id`);
